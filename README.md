# Nextcloud

Différentes choses autour de Nextcloud

## Conférences

[Support de conférences autour de Nextcloud](https://framagit.org/genma/nextcloud/tree/master/Conferences) sous licence CC-BY-SA

* [Nextcloud - Le cloud respectueux de nos données.pdf ](https://framagit.org/genma/nextcloud/blob/master/Conferences/Nextcloud%20-%20Le%20cloud%20respectueux%20de%20nos%20donn%C3%A9es/Nextcloud%20-%20Le%20cloud%20respectueux%20de%20nos%20donn%C3%A9es.pdf) [Source](https://framagit.org/genma/nextcloud/tree/master/Conferences/Nextcloud%20-%20Le%20cloud%20respectueux%20de%20nos%20donn%C3%A9es)

## Veille

Mois après mois, une série d'annonces, liens, articles trouvés autour de l'actualité de Nextcloud

* [Nextcloud - Veille Novembre 2019](https://framagit.org/genma/nextcloud/blob/master/Veille/Nextcloud_Veille_Novembre_2019.md)
 
## Wiki

Une page / série de liens vers des tutoriels etc.
* [Wiki Nextcloud](https://framagit.org/genma/nextcloud/blob/master/Wiki/Wiki_Nextcloud.md)
