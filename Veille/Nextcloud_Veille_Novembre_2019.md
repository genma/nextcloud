## Collabora Mattermost Plugin

*Collabora Mattermost plugin allows Mattermost users to view and edit files directly in Mattermost. The plugin is open-source, you can find it here on github.*

* Lien : [https://www.collaboraoffice.com](https://www.collaboraoffice.com/integrations/collabora-mattermost-plugin/)
* Niveau : Article
* Hashtag : #Annonce #Nextcloud #Mattermost #Plugin

## Arawa annonce le déploiement de Collabora Online à l’Université de Lille

*Intégrée de manière transparente à Nextcloud, la plateforme de partage de fichiers et de collaboration en ligne, l’Université de Lille a mis en place un service visant à simplifier les projets de collaboration entre les étudiants mais également entre étudiants et personnels de l’université : Collabora Online est désormais mis à la disposition de plus de 70 000 personnes afin qu’elles puissent travailler ensemble sur des standards communs en termes d’outils bureautiques en ligne.*

* Lien : [Arawa.fr](https://www.arawa.fr/2019/11/23/arawa-annonce-le-deploiement-de-collabora-online-a-luniversite-de-lille/)
* Niveau : Article
* Hashtag : #Nextcloud #Collabora #Arawa #Université

## Collabora Online Plugin for Moodle

*the Collabora Online plugin for Moodle, bringing new possibilities to the popular open source learning platform: life editing of and cooperating on office documents.*

* Lien : [Collaboraoffice.com](https://www.collaboraoffice.com/integrations/collabora-online-plugin-for-moodle/)
* Niveau : Annonce
* Hashtag : #Collabora #Moodle #Nextcloud

## Pico CMS for Nextcloud 1.0 is released. 

*Pico CMS for Nextcloud combines the power of Pico and Nextcloud to create simple, secure, shareable and amazingly powerful websites with just a few clicks. Pico is a stupidly simple, blazing fast, flat file CMS - making the web easy!*

* Lien : [https://apps.nextcloud.com](https://apps.nextcloud.com/apps/cms_pico)
* Niveau : Annonce
* Hashtag : #PicoCMS #Website #Nextcloud #Apps

